package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Person;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class PersonMapper extends Mapper {
    public PersonMapper(DataService provider) {
		super(provider);
	}

	@Override
    public Person mapFromStorage(ResultSet rs) throws SQLException {
        Person person = new Person();
        person.setId(rs.getLong("id_person"));
        person.setName(rs.getString("name"));
        person.setPatronymic(rs.getString("patronymic"));
        person.setSurname(rs.getString("surname"));
        person.setBirthdate(rs.getTimestamp("birthdate").toLocalDateTime());
        person.setEmail(rs.getString("email"));
        person.setPhone(rs.getLong("phone"));
        
        return person;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		Person person = (Person) object;
		Map<String, Object> values = new HashMap<>();
		
		values.put("id_person", person.getId());
		values.put("name", person.getName());
		values.put("patronymic", person.getPatronymic());
		values.put("surname", person.getSurname());
		values.put("birthdate", person.getBirthdate());
		values.put("email", person.getEmail());
		values.put("phone", person.getPhone());
		
		return values;
	}
}
