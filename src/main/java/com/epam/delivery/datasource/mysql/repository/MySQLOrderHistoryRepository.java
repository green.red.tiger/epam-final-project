package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Order;
import com.epam.delivery.datasource.mysql.MySQLDataService;
import com.epam.delivery.data.domain.OrderHistory;
import com.epam.delivery.data.repository.OrderHistoryRepository;
import com.epam.delivery.datasource.mysql.mapper.OrderHistoryMapper;

public class MySQLOrderHistoryRepository extends MySQLAbstractRepository<OrderHistory> implements OrderHistoryRepository {
    
	private MySQLOrderHistoryRepository(MySQLOrderHistoryRepository other) {
		super(other);
	}
	public MySQLOrderHistoryRepository(MySQLDataService provider) {
		super(provider, "order_history", new OrderHistoryMapper(provider));
	}
	
	@Override
	public OrderHistoryRepository filterByOrder(Order order) {
		long orderId = order.getId();
		
		return (OrderHistoryRepository)filterByFieldValue("id_order", orderId);
	}

	@Override
	public MySQLOrderHistoryRepository clone() {
		return new MySQLOrderHistoryRepository(this);
	}
}
