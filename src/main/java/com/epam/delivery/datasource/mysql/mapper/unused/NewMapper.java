package com.epam.delivery.datasource.mysql.mapper.unused;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.epam.delivery.core.entity.Attribute;
import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.datasource.mysql.MySQLDataSource;
import com.epam.delivery.service.DataService;

public class NewMapper {
	public static <C extends Storable> Storable mapFromStorage(Class<C> clazz, DataService provider,
			ResultSet rs) throws SQLException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Storable stor = clazz.getConstructor().newInstance(null);
		for (Field f : clazz.getFields()) {
			if (f.isAnnotationPresent(Attribute.class)) {
				String attrName = f.getAnnotation(Attribute.class).value();
				if ("".equals(attrName)) {
					attrName = f.getName().toLowerCase();
				}
				String setterName = "set" + attrName.substring(0, 1).toUpperCase() + attrName.substring(1);
				Method setter = clazz.getMethod(setterName);
				

			}
		}
		return null;
	}

	public static Map<String, String> mapToStorage(Storable storable) throws SQLException {

		return null;
	}
}
