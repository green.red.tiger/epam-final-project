package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Comment;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class CommentMapper extends Mapper {
    public CommentMapper(DataService provider) {
		super(provider);
	}

	@Override
    public Comment mapFromStorage(ResultSet rs) throws SQLException {
        Comment comment = new Comment();
        comment.setId(rs.getLong("id_comment"));
        comment.setAuthor(getAsReference(rs, "author", provider.getPersonRepository()));
        comment.setCreationDate(rs.getTimestamp("creation_date").toLocalDateTime());
        comment.setEditDate(rs.getTimestamp("edit_date").toLocalDateTime());
        comment.setText(rs.getString("text"));
                
        return comment;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		Comment comment = (Comment) object;
		Map<String, Object> values = new HashMap<>();
		
		values.put("id_comment", comment.getId());
		values.put("author", comment.getAuthor().getId());
		values.put("creation_date", comment.getCreationDate());
		values.put("edit_date", comment.getEditDate());
		values.put("text", comment.getText());
		return values;
	}
}
