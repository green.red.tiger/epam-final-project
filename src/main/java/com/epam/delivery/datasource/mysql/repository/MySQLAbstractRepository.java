package com.epam.delivery.datasource.mysql.repository;

import java.util.List;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.core.entity.StorageState;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.datasource.mysql.MySQLDataService;
import com.epam.delivery.datasource.mysql.MySQLDataSource;
import com.epam.delivery.util.Either;
import com.epam.delivery.util.Empty;

public abstract class MySQLAbstractRepository<T extends Storable> implements GenericRepository<T>, Cloneable {
	protected MySQLDataSource<T> dataSource;
	protected String tableName;
	
	public MySQLAbstractRepository(MySQLDataService provider, String tableName, Mapper mapper) {
		this.tableName = tableName;
		dataSource = new MySQLDataSource<T>(provider.getConnection(), tableName, mapper);
	}
	
	protected MySQLAbstractRepository(MySQLAbstractRepository<T> other) {
		this.tableName = other.tableName;
		dataSource = new MySQLDataSource<T>(other.dataSource);
	}
	
	@Override
	public Reference<T> getReference(T value) {
		return new Reference<>(value, this);
	}
	@Override
	public Reference<T> getReference(long id) {
		return new Reference<>(id, this);
	}
	
	@Override
	public Either<T, Exception> get(long id) {
		MySQLDataSource<T> idDataSource = new MySQLDataSource<>(dataSource);
		idDataSource.addCondition(String.format("id_%s = %s", tableName, id));
		return idDataSource.selectOne();
	}
	
	@Override
	public Either<T, Exception> getOnly() {
		return dataSource.selectOne();
	}

	@Override
	public Either<List<T>, Exception> getAll() {
		return dataSource.selectAll();
	}

	@Override
	public Either<Long, Exception> add(T entity) {
		entity.actualizeRefs();
		Either<Long, Exception> result = dataSource.insert(entity);
		if (result.isNormal()) {
			entity.setStorageState(StorageState.ACTUAL);
			entity.setId(result.getNormal());
		}
		
		return result;
	}

	@Override
	public Either<Empty, Exception> update(T entity) {
		entity.actualizeRefs();
		
		Either<Empty, Exception> result = dataSource.update(entity);
		if (result.isNormal()) {
			entity.setStorageState(StorageState.ACTUAL);
		}
		return result;
	}

	@Override
	public Either<Empty, Exception> remove(T entity) {
		return dataSource.delete(entity);

	}

	@Override
	public GenericRepository<T> limit(int from, int to) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public abstract MySQLAbstractRepository<T> clone();
	
	protected MySQLAbstractRepository<T> filterByFieldValue(String fieldName, Object value) {
		String condition = String.format("%s = %s", value.toString());
		MySQLAbstractRepository<T> other = clone();
		other.dataSource.addCondition(condition);
		
		return other;
	}
}
