package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.ListReference;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Order;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class OrderMapper extends Mapper {
    public OrderMapper(DataService provider) {
        super(provider);
    }

    @Override
    public Order mapFromStorage(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setId(rs.getLong("id_order"));
        order.setCargo(getAsReference(rs, "id_cargo", provider.getCargoRepository()));
        order.setExternalId(rs.getLong("external_id"));
        order.setFrom(getAsReference(rs, "from" , provider.getWaypointRepository()));
        order.setTo(getAsReference(rs, "to" , provider.getWaypointRepository()));
        order.setCreator(getAsReference(rs, "creator", provider.getPersonRepository()));
        order.setSender(getAsReference(rs, "sender", provider.getPersonRepository()));
        order.setReceiver(getAsReference(rs, "receiver", provider.getPersonRepository()));
        order.setInvoice(getAsReference(rs, "id_invoice", provider.getInvoiceRepository()));
                
        order.setHistory(new ListReference<>(provider.getOrderHistoryRepository().filterByOrder(order)));
        
        return order;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object){
		Order order = (Order) object;
		Map<String, Object> values = new HashMap<>();
		
		values.put("id_order", order.getId());
		values.put("id_cargo", order.getCargo().getId());
		values.put("external_id", order.getExternalId());
		values.put("from", order.getFrom().getId());
		values.put("to", order.getTo().getId());
		values.put("creator", order.getCreator().getId());
		values.put("sender", order.getSender().getId());
		values.put("receiver", order.getReceiver().getId());
		values.put("id_invoice", order.getInvoice().getId());
		
		return values;
	}
}
