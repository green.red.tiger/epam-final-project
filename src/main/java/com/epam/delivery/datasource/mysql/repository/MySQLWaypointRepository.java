package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Waypoint;
import com.epam.delivery.data.repository.WaypointRepository;
import com.epam.delivery.datasource.mysql.mapper.WaypointMapper;
import com.epam.delivery.datasource.mysql.MySQLDataService;

public class MySQLWaypointRepository extends MySQLAbstractRepository<Waypoint> implements WaypointRepository {
    
	@SuppressWarnings("unused")
	private MySQLWaypointRepository(MySQLWaypointRepository other) {
		super(other);
	}
	
	public MySQLWaypointRepository(MySQLDataService provider) {
		super(provider, "waypoint", new WaypointMapper(provider));
	}

	@Override
	public MySQLWaypointRepository clone() {
		return new MySQLWaypointRepository(this);
	}
}
