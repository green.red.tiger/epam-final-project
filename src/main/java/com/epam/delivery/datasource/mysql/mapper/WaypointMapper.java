package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Waypoint;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class WaypointMapper extends Mapper {
    public WaypointMapper(DataService provider) {
		super(provider);
	}

	@Override
    public Waypoint mapFromStorage(ResultSet rs) throws SQLException {
        Waypoint waypoint = new Waypoint();
        waypoint.setId(rs.getLong("id_waypoint"));
        waypoint.setCity(getAsReference(rs, "id_city", provider.getCityRepository()));
        waypoint.setLongitude(rs.getLong("longitude"));
        waypoint.setLatitude(rs.getLong("latitude"));
        waypoint.setAddress(rs.getString("address"));
        
        return waypoint;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object){
		Waypoint waypoint = (Waypoint) object;
		Map<String, Object> values = new HashMap<>();
		
		values.put("id_comment", waypoint.getId());
		values.put("id_city", waypoint.getCity().getId());
		values.put("longitude", waypoint.getLongitude());
		values.put("latitude", waypoint.getLatitude());
		values.put("address", waypoint.getAddress());
		
		return values;
	}
}
