package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Invoice;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class InvoiceMapper extends Mapper {
    public InvoiceMapper(DataService provider) {
		super(provider);
	}

	@Override
    public Invoice mapFromStorage(ResultSet rs) throws SQLException {
        Invoice invoice = new Invoice();
        invoice.setId(rs.getLong("id_invoice"));
        invoice.setExternalId(rs.getLong("external_id"));
        invoice.setTotalPrice(rs.getBigDecimal("total_price"));
        invoice.setPaid(rs.getBoolean("is_paid"));
        
        return invoice;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		Invoice invoice = (Invoice) object;
		Map<String, Object> values = new HashMap<>();
		values.put("id_invoice", invoice.getId());
		values.put("external_id", invoice.getExternalId());
		values.put("total_price", invoice.getTotalPrice().toPlainString());
		values.put("is_paid", invoice.isPaid() ? 1 : 0);
		return values;
	}
}
