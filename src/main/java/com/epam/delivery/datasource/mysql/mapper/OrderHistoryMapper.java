package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.OrderHistory;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class OrderHistoryMapper extends Mapper {
    public OrderHistoryMapper(DataService provider) {
		super(provider);
	}

	@Override
    public OrderHistory mapFromStorage(ResultSet rs) throws SQLException {
        OrderHistory orderHistory = new OrderHistory();
        orderHistory.setId(rs.getLong("id_order_history"));
        
        orderHistory
            .setCurrentLocation(getAsReference(rs, "current_location", provider.getWaypointRepository()));
        
        orderHistory.setState(rs.getInt("state"));
        orderHistory.setDate(rs.getDate("date")
            .toLocalDate());
        orderHistory.setComment(getAsReference(rs, "id_comment", provider.getCommentRepository()));

        return orderHistory;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		OrderHistory oh = (OrderHistory) object;
		Map<String, Object> values = new HashMap<>();
		
		values.put("id_order_history", oh.getId());
		values.put("current_location", oh.getCurrentLocation().getId());
		values.put("state", oh.getState());
		values.put("date", oh.getDate());
		values.put("comment", oh.getComment().getId());
		
		return values;
	}
}
