package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.City;
import com.epam.delivery.data.repository.CityRepository;
import com.epam.delivery.datasource.mysql.MySQLDataService;
import com.epam.delivery.datasource.mysql.mapper.CityMapper;

public class MySQLCityRepository extends MySQLAbstractRepository<City> implements CityRepository {
//	@SuppressWarnings("unused")
	private MySQLCityRepository(MySQLCityRepository other) {
		super(other);
	}
	public MySQLCityRepository(MySQLDataService provider) {
		super(provider, "city", new CityMapper(provider));
	}
	@Override
	public MySQLAbstractRepository<City> clone() {
		return new MySQLCityRepository(this);
	}
}
