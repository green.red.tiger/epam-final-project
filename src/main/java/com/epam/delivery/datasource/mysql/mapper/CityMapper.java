package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.City;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class CityMapper extends Mapper {
    public CityMapper(DataService provider) {
		super(provider);
	}

	@Override
    public City mapFromStorage(ResultSet rs) throws SQLException {
        City city = new City();
        city.setId(rs.getLong("id_city"));
        city.setValue(rs.getString("value"));
        return city;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		City city = (City) object;
		Map<String, Object> values = new HashMap<>();
		values.put("id_city", city.getId());
		values.put("value", city.getValue());
		return values;
	}
}
