package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Comment;
import com.epam.delivery.data.repository.CommentRepository;
import com.epam.delivery.datasource.mysql.mapper.CommentMapper;
import com.epam.delivery.datasource.mysql.MySQLDataService;

public class MySQLCommentRepository extends MySQLAbstractRepository<Comment> implements CommentRepository {
//	@SuppressWarnings("unused")
	private MySQLCommentRepository(MySQLCommentRepository other) {
		super(other);
	}
	public MySQLCommentRepository(MySQLDataService provider) {
		super(provider, "order", new CommentMapper(provider));
	}

	@Override
	public MySQLAbstractRepository<Comment> clone() {
		return new MySQLCommentRepository(this);
	}
}
