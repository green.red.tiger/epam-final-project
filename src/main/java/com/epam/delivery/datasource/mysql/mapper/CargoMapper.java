package com.epam.delivery.datasource.mysql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.data.domain.Cargo;
import com.epam.delivery.data.domain.CargoType;
import com.epam.delivery.datasource.mysql.Mapper;
import com.epam.delivery.service.DataService;

public class CargoMapper extends Mapper {
    public CargoMapper(DataService provider) {
		super(provider);
	}

	@Override
    public Cargo mapFromStorage(ResultSet rs) throws SQLException {
        Cargo cargo = new Cargo();
        cargo.setId(rs.getLong("id_cargo"));
        cargo.setMass(rs.getBigDecimal("mass"));
        cargo.setVolume(rs.getBigDecimal("volume"));
        cargo.setType(CargoType.valueOf(rs.getInt("id_cargo_type")));
        
        return cargo;
    }

	@Override
	public Map<String, Object> mapToStorage(Storable object) {
		Cargo cargo = (Cargo) object;
		Map<String, Object> values = new HashMap<>();
		values.put("id_cargo", cargo.getId());
		values.put("mass", cargo.getMass().toPlainString());
		values.put("volume", cargo.getVolume().toPlainString());
		values.put("id_cargo_type", cargo.getType().getValue());
		return values;
	}
}
