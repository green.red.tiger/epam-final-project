package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Person;
import com.epam.delivery.data.repository.PersonRepository;
import com.epam.delivery.datasource.mysql.mapper.OrderMapper;
import com.epam.delivery.datasource.mysql.MySQLDataService;

public class MySQLPersonRepository extends MySQLAbstractRepository<Person> implements PersonRepository {
	@SuppressWarnings("unused")
	private MySQLPersonRepository(MySQLPersonRepository other) {
		super(other);
	}
	public MySQLPersonRepository(MySQLDataService provider) {
		super(provider, "person", new OrderMapper(provider));
	}

	@Override
	public MySQLAbstractRepository<Person> clone() {
		return new MySQLPersonRepository(this);
	}
}
