package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.datasource.mysql.MySQLDataService;

import com.epam.delivery.data.domain.Order;
import com.epam.delivery.data.repository.OrderRepository;
import com.epam.delivery.datasource.mysql.mapper.OrderMapper;

public class MySQLOrderRepository extends MySQLAbstractRepository<Order> implements OrderRepository {
    
	private MySQLOrderRepository(MySQLOrderRepository other) {
		super(other);
	}
	public MySQLOrderRepository(MySQLDataService provider) {
		super(provider, "order", new OrderMapper(provider));
	}

	@Override
	public OrderRepository filterByExternalId(long externalId) {
//		MySQLDataSource<Order> externalIdDataSource = new MySQLDataSource<Order>(dataSource);
//		externalIdDataSource.addCondition(String.format("external_id = %s", externalId));
//		MySQLOrderRepository2 filtered = new MySQLOrderRepository2();
//		filtered.dataSource = externalIdDataSource;
//		
//		return filtered;
		return this;
	}

	@Override
	public MySQLAbstractRepository<Order> clone() {
		return new MySQLOrderRepository(this);
	}



}
