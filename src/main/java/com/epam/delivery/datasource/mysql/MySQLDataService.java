package com.epam.delivery.datasource.mysql;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.epam.delivery.datasource.mysql.repository.MySQLCargoRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLCityRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLCommentRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLInvoiceRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLOrderHistoryRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLOrderRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLPersonRepository;
import com.epam.delivery.datasource.mysql.repository.MySQLWaypointRepository;
import com.epam.delivery.service.DataService;

public class MySQLDataService implements DataService {
//    private Connection connection;
	private DataSource dataSource;
    
    private MySQLCargoRepository cargoRepository;
    private MySQLCommentRepository commentRepository;
    private MySQLInvoiceRepository invoiceRepository;
    private MySQLOrderRepository orderRepository;
    private MySQLOrderHistoryRepository orderHistoryRepository;
    private MySQLPersonRepository personRepository;
    private MySQLWaypointRepository waypointRepository;
    private MySQLCityRepository cityRepository;
    
    public MySQLDataService() throws NamingException, SQLException {
        	InitialContext initContext= new InitialContext();
        	dataSource = (DataSource) initContext.lookup("java:comp/env/jdbc/DeliveryDB"); 
            
            cargoRepository = new MySQLCargoRepository(this);
            commentRepository = new MySQLCommentRepository(this);
            invoiceRepository = new MySQLInvoiceRepository(this);
            orderRepository = new MySQLOrderRepository(this);
            orderHistoryRepository = new MySQLOrderHistoryRepository(this);
            personRepository = new MySQLPersonRepository(this);
            waypointRepository = new MySQLWaypointRepository(this);
            cityRepository = new MySQLCityRepository(this);
            
            //orderRepository = new MySQLOrderRepository(this);
    }

	public Connection getConnection() {
        try {
        	return dataSource.getConnection();
        }
        catch (Exception e) {
        	return null;
        }
    }

    @Override
	public MySQLCargoRepository getCargoRepository() {
        return cargoRepository;
    }

    @Override
	public MySQLCommentRepository getCommentRepository() {
        return commentRepository;
    }

    @Override
	public MySQLInvoiceRepository getInvoiceRepository() {
        return invoiceRepository;
    }

    @Override
	public MySQLOrderRepository getOrderRepository() {
        return orderRepository;
    }

    @Override
	public MySQLOrderHistoryRepository getOrderHistoryRepository() {
        return orderHistoryRepository;
    }

    @Override
	public MySQLPersonRepository getPersonRepository() {
        return personRepository;
    }

    @Override
	public MySQLWaypointRepository getWaypointRepository() {
        return waypointRepository;
    }

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	@Override
	public MySQLCityRepository getCityRepository() {
		return cityRepository;
	}
    
}
