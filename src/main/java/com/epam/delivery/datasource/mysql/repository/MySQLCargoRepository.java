package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Cargo;
import com.epam.delivery.data.repository.CargoRepository;
import com.epam.delivery.datasource.mysql.MySQLDataService;
import com.epam.delivery.datasource.mysql.mapper.CargoMapper;

public class MySQLCargoRepository extends MySQLAbstractRepository<Cargo> implements CargoRepository {
//	@SuppressWarnings("unused")
	private MySQLCargoRepository(MySQLCargoRepository other) {
		super(other);
	}
	public MySQLCargoRepository(MySQLDataService provider) {
		super(provider, "cargo", new CargoMapper(provider));
	}

	@Override
	public MySQLAbstractRepository<Cargo> clone() {
		return new MySQLCargoRepository(this);
	}
}
