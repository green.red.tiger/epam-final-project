package com.epam.delivery.datasource.mysql.repository;

import com.epam.delivery.data.domain.Invoice;
import com.epam.delivery.data.repository.InvoiceRepository;
import com.epam.delivery.datasource.mysql.mapper.InvoiceMapper;
import com.epam.delivery.datasource.mysql.MySQLDataService;

public class MySQLInvoiceRepository extends MySQLAbstractRepository<Invoice> implements InvoiceRepository {
//	@SuppressWarnings("unused")
	private MySQLInvoiceRepository(MySQLInvoiceRepository other) {
		super(other);
	}
	public MySQLInvoiceRepository(MySQLDataService provider) {
		super(provider, "invoice", new InvoiceMapper(provider));
	}

	@Override
	public MySQLAbstractRepository<Invoice> clone() {
		return new MySQLInvoiceRepository(this);
	}
}
