package com.epam.delivery.datasource.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.service.DataService;

public abstract class Mapper {
    protected DataService provider;

    protected Mapper(DataService provider) {
        this.provider = provider;
    }

    public abstract Storable mapFromStorage(ResultSet rs) throws SQLException;
    public abstract Map<String, Object> mapToStorage(Storable object);
    

    protected <T extends Storable> Reference<T> getAsReference(ResultSet rs, String fieldName,
            GenericRepository<T> repository) throws SQLException {
        return new Reference<>(rs.getLong(fieldName), repository);
    }
}
