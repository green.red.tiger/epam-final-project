package com.epam.delivery.datasource.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.sql.ResultSet;
import java.sql.Statement;

import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.exception.DataNotFoundException;
import com.epam.delivery.util.Either;
import com.epam.delivery.util.Empty;
import com.epam.delivery.util.ExceptionWrapper;
import com.epam.delivery.util.ThrowingFunction;
import com.epam.delivery.util.ThrowingSupplier;

public class MySQLDataSource<S extends Storable> {
	private Connection connection;
	private Mapper mapper;
	private String tableName;
	private Map<String, Boolean> fieldMap = new HashMap<>();
	private List<String> conditionList = new ArrayList<>();

	private String selectQuery;

	public MySQLDataSource(Connection connection, String tableName, Mapper mapper) {
		this.connection = connection;
		this.tableName = tableName;
		this.mapper = mapper;
	}

	public MySQLDataSource(MySQLDataSource<S> other) {
		connection = other.connection;
		mapper = other.mapper;
		tableName = other.tableName;
		fieldMap = new HashMap<String, Boolean>(other.fieldMap);
		conditionList = new ArrayList<>(other.conditionList);
	}

	public void addField(String field, boolean isWritable) {
		fieldMap.put(field, isWritable);
		selectQuery = null;
	}

	public void addCondition(String condition) {
		conditionList.add(condition);
		selectQuery = null;
	}

	public Either<List<S>, Exception> selectAll() {
		if (selectQuery == null) {
			selectQuery = buildSelectQuery();
		}
		ThrowingSupplier<PreparedStatement> ps = () -> connection.prepareStatement(selectQuery);
		ThrowingFunction<PreparedStatement, List<S>> body = st -> {
			ResultSet resultSet = st.executeQuery();

			List<S> resultList = new ArrayList<>();
			while (resultSet.next()) {
				@SuppressWarnings("unchecked")
				S object = (S) mapper.mapFromStorage(resultSet);

				resultList.add((object));
			}

			return resultList;
		};

		return ExceptionWrapper.tryWith(ps, body);
	}

	public Either<S, Exception> selectOne() {
		return selectAll().collapse(list -> Either.test(list.size() != 0, list, new DataNotFoundException()))
				.collapse(list -> Either.test(list.size() == 1, list, new IllegalStateException()))
				.then(list -> list.get(0));
	}

	public Either<Long, Exception> insert(S object) {
		String query = buildInsertQuery(object);
		ThrowingSupplier<Statement> ps = () -> connection.createStatement();
		ThrowingFunction<Statement, Long> body = st -> {
			st.executeUpdate(query);
			ResultSet resultSet = st.executeQuery("SELECT LAST_INSERT_ID()");
			resultSet.next();
			return resultSet.getLong(1);
		};

		return ExceptionWrapper.tryWith(ps, body);

	}

	public Either<Empty, Exception> update(S object) {
		String query = buildUpdateQuery(object);
		ThrowingSupplier<Statement> ps = () -> connection.createStatement();
		ThrowingFunction<Statement, Empty> body = st -> {
			st.executeUpdate(query);
			return Empty.get();
		};

		return ExceptionWrapper.tryWith(ps, body);

	}

	public Either<Empty, Exception> delete(S object) {
		String query = buildDeleteQuery(object);
		ThrowingSupplier<Statement> ps = () -> connection.createStatement();
		ThrowingFunction<Statement, Empty> body = st -> {
			st.executeUpdate(query);
			return Empty.get();
		};

		return ExceptionWrapper.tryWith(ps, body);

	}

	private String buildSelectQuery() {
		String fieldSet = fieldMap.isEmpty() ? "*" : String.join(", ", fieldMap.keySet());
		String query = String.format("SELECT %s FROM `%s`", fieldSet, tableName);

		if (!conditionList.isEmpty()) {
			query += " WHERE " + String.join(" AND ", conditionList);
		}

		return query;
	}

	private String buildInsertQuery(S object) {
		Map<String, Object> values = mapper.mapToStorage(object);
		values.remove("id_" + tableName);

		String head = String.format("INSERT INTO `%s` (%s) VALUES ", tableName,
				String.join(", ", values.keySet().stream().map(s -> "`" + s + "`").collect(Collectors.toList())));
		String body = "(" + values.values().stream().map(Object::toString).map(s -> String.format("'%s'", s))
				.collect(Collectors.joining(" ,")) + ")";

		return head + body;

	}

	private String buildUpdateQuery(S object) {
		Map<String, Object> values = mapper.mapToStorage(object);
		String head = String.format("UPDATE `%s` SET ", tableName);
		String body = values.entrySet().stream().map(e -> String.format("%s = %s", e.getKey(), e.getValue()))
				.collect(Collectors.joining(" , "));
		String tail = String.format(" WHERE id_%s = %s", tableName, object.getId());

		return head + body + tail;
	}

	private String buildDeleteQuery(S object) {
		return String.format("DELETE FROM `%s` WHERE id_%s = %s", tableName, tableName, object.getId());
	}
}
