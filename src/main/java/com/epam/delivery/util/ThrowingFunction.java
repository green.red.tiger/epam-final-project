package com.epam.delivery.util;

@FunctionalInterface
public interface ThrowingFunction<T,S> {
	public S apply(T input) throws Exception;
}
