package com.epam.delivery.util;

public class ExceptionWrapper {
	private ExceptionWrapper() {
		
	}
	
	public static <N,R extends AutoCloseable> Either<N, Exception> tryWith(ThrowingSupplier<R> resource, ThrowingFunction<R, N> body) {
		try (R res = resource.get()) {
			return Either.makeNormal(body.apply(res));
		}
		catch (Exception e) {
			return Either.makeAlt(e);
		}
	}
	
	public static <N> Either<N, Exception> try$(ThrowingSupplier<N> body) {
		try {
			return Either.makeNormal(body.get());
		}
		catch (Exception e) {
			return Either.makeAlt(e);
		}
	}
}
