package com.epam.delivery.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class Either<N, A> {
    private Object value;
    private boolean isNormal;

    protected Either(Object value) {
        this.value = value;
    }

    public static <N, A> Either<N, A> makeNormal(N normal) {
        Either<N, A> either = new Either<>(normal);
        either.isNormal = true;
        return either;
    }

    public static <N, A> Either<N, A> makeAlt(A alt) {
        return new Either<>(alt);
    }

    public boolean isNormal() {
        return isNormal;
    }

    public boolean isAlt() {
        return !isNormal;
    }

    @SuppressWarnings("unchecked")
    public N getNormal() {
        if (isNormal()) {
            return (N) value;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public A getAlt() {
        if (isAlt()) {
            return (A) value;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <N1, A1> Either<N1, A1> map(Function<N, N1> onNormal,
            Function<A, A1> onAlt) {
        if (isNormal()) {
            N1 newValue = onNormal.apply((N) value);
            return makeNormal(newValue);
        }

        A1 newValue = onAlt.apply((A) value);
        return makeAlt(newValue);
    }

    @SuppressWarnings("unchecked")
    public <Y> Y reduce(Function<N, Y> onNormal, Function<A, Y> onAlt) {
        if (isNormal()) {
            return onNormal.apply((N) value);
        }

        return onAlt.apply((A) value);

    }
    
    @SuppressWarnings("unchecked")
    public Optional<N> toOptional() {
        return isNormal ? Optional.of((N)value) : Optional.empty();
    }
    
    public static <N,A> Either<N,A> fromOptional(Optional<N> value, Supplier<A> onEmpty) {
        if (value.isPresent()) {
            return makeNormal(value.get());
        }
        return makeAlt(onEmpty.get());
    }
    
    public static <N,A> Either<N,A> test(boolean testValue, N normalValue, A altValue) {
    	if (testValue) {
    		return makeNormal(normalValue);
    	} else {
    		return makeAlt(altValue);
    	}
    }
    
    @SuppressWarnings("unchecked")
    public <Y> Either<Y,A> then(Function<N, Y> normalMapper) {
        if (isNormal) {
            return Either.makeNormal(normalMapper.apply((N)value));
        }
        return Either.makeAlt((A)value);
    }
    
    @SuppressWarnings("unchecked")
    public <Y> Either<Y,A> collapse(Function<N, Either<Y,A>> normalMapper) {
        if (isNormal) {
            return normalMapper.apply((N)value);
        }
        return Either.makeAlt((A) value);
        
        
    }
    
    @SuppressWarnings("unchecked")
	public Either<A,N> swap() {
    	return isNormal ? makeNormal((A) value) : makeAlt((N) value);
    }
}
