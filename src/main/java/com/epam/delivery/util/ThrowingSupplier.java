package com.epam.delivery.util;

@FunctionalInterface
public interface ThrowingSupplier<T> {
	public T get() throws Exception;
}
