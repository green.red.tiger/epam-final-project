package com.epam.delivery.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.delivery.core.Action;
import com.epam.delivery.core.UserRole;
import com.epam.delivery.core.View;
import com.epam.delivery.service.ActionService;

public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) {
		Action action = ActionService.get().findActionByRequest(req);
		View view = action.perform(req, UserRole.CUSTOMER);
		try {
			view.show(req, getServletContext(), resp);
		} catch (Exception e) {

		}

	}

}