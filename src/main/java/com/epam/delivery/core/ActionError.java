package com.epam.delivery.core;

import lombok.Getter;

public class ActionError {
    @Getter
    private String message;

    public ActionError(String message) {
        super();
        this.message = message;
    }
    
    public static ActionError fromException(Exception e) {
        return new ActionError(e.toString());
    }
    
}
