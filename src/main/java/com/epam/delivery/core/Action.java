package com.epam.delivery.core;

import com.epam.delivery.service.ViewService;
import com.epam.delivery.util.Either;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.servlet.ServletRequest;

@Data
public class Action {
    private Function<ServletRequest, Either<Map<String, InputDTO>, List<ActionError>>> inputMapper;
    private Set<UserRole> allowList;
    private Command command;
    private String successViewName;

    public View perform(ServletRequest request, UserRole currentUserRole) {
        if (!allowList.contains(currentUserRole)) {
            return ViewService.getNoAccessView();
        }
 
        return inputMapper.apply(request)
        .map(command::perform, ViewService::getErrorView)
        .reduce(e -> ViewService.getView(successViewName, e), v -> v);
    }
}
