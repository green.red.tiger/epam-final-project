package com.epam.delivery.core.entity;

public abstract class Storable {
	private StorageState storageState = StorageState.NEW;

	public StorageState getStorageState() {
		return storageState;
	}

	public void setStorageState(StorageState storageState) {
		this.storageState = storageState;
	}
	
	public abstract long getId();
	public abstract void setId(long id);
	
	public abstract void actualizeRefs();
}
