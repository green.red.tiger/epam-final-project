package com.epam.delivery.core.entity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.epam.delivery.util.Either;

public class ListReference<S extends Storable> {
    private GenericRepository<S> source;
    
    public ListReference(GenericRepository<S> source) {
        this.source = source;
    }
    
    public Either<List<S>,Exception> resolve() {
        return source.getAll();
    }
}
