package com.epam.delivery.core.entity;

import java.util.List;

import com.epam.delivery.util.Either;
import com.epam.delivery.util.Empty;

public interface GenericRepository<T extends Storable> {
	Reference<T> getReference(T value);
	Reference<T> getReference(long id);
	
	Either<T, Exception> get(long id);
	
	Either<T, Exception> getOnly();
	Either<List<T>, Exception> getAll();
	
	Either<Long,Exception> add(T entity);
	Either<Empty, Exception> update(T entity);
	Either<Empty, Exception> remove(T entity);
	
	GenericRepository<T> limit(int from, int to);
}
