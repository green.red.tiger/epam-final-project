package com.epam.delivery.core.entity;

public enum StorageState {
	NEW,
	MODIFIED,
	ACTUAL
}
