package com.epam.delivery.core.entity;

import com.epam.delivery.util.Either;

public class Reference<S extends Storable> {
	private Either<S, Long> value;
	GenericRepository<S> repository;

	public Reference(S value, GenericRepository<S> repository) {
		this.value = Either.makeNormal(value);
		this.repository = repository;
	}

	public Reference(Long value, GenericRepository<S> repository) {
		this.value = Either.makeAlt(value);
		this.repository = repository;
	}

	public Either<S, Exception> resolve() {
		Either<S, Exception> resolved = value.reduce(Either::makeNormal, repository::get);
		value = resolved.map(n -> n, e -> value.getAlt());
		return resolved;
	}

	public long getId() {
		return value.reduce(Storable::getId, e -> e);
	}

	public void actualize() {
		if (value.isNormal()) {
			S storable = value.getNormal();
			switch (storable.getStorageState()) {
			case NEW:
				repository.add(storable);
				break;
			case MODIFIED:
				repository.update(storable);
				break;
			default:
				break;
			}

		}
	}
}
