package com.epam.delivery.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.delivery.util.Either;

public abstract class Command {
    public abstract Either<OutputDTO, List<ActionError>> perform(
            Map<String, InputDTO> input);
    
    protected static List<ActionError> exceptionToErrorList(Exception e) {
        List<ActionError> errorList = new ArrayList<>();
        errorList.add(ActionError.fromException(e));
        return errorList;
    }
}
