package com.epam.delivery.core;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface View {
    void show(ServletRequest request, ServletContext context, HttpServletResponse response) throws IOException, ServletException;
}
