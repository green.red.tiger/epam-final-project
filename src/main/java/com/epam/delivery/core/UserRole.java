package com.epam.delivery.core;

public enum UserRole {
    GUEST,
    CUSTOMER,
    MANAGER
}
