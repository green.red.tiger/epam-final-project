package com.epam.delivery.service;

import java.sql.SQLException;
import java.util.Objects;

import javax.naming.NamingException;

import com.epam.delivery.datasource.mysql.MySQLDataService;

public class ServiceFactory {
	static DataService dataService;
	
	public static DataService getDataService() {
		if (Objects.isNull(dataService)) {
			try {
				dataService = new MySQLDataService();
			} catch (NamingException | SQLException e) {
				// TODO Auto-generated catch block
				
			}
		}
		
		return dataService;
	}
}
