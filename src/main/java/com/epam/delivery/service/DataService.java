package com.epam.delivery.service;

import javax.sql.DataSource;

import com.epam.delivery.data.repository.CargoRepository;
import com.epam.delivery.data.repository.CityRepository;
import com.epam.delivery.data.repository.CommentRepository;
import com.epam.delivery.data.repository.InvoiceRepository;
import com.epam.delivery.data.repository.OrderHistoryRepository;
import com.epam.delivery.data.repository.OrderRepository;
import com.epam.delivery.data.repository.PersonRepository;
import com.epam.delivery.data.repository.WaypointRepository;

public interface DataService {
	CargoRepository getCargoRepository();

	CommentRepository getCommentRepository();

	InvoiceRepository getInvoiceRepository();

	OrderRepository getOrderRepository();

	OrderHistoryRepository getOrderHistoryRepository();

	PersonRepository getPersonRepository();

	WaypointRepository getWaypointRepository();

	DataSource getDataSource();

	CityRepository getCityRepository();

}