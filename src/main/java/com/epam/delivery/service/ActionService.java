package com.epam.delivery.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.servlet.ServletRequest;

import com.epam.delivery.core.Action;
import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.Command;
import com.epam.delivery.core.InputDTO;
import com.epam.delivery.core.UserRole;
import com.epam.delivery.data.DTO.input.ExternalId;
import com.epam.delivery.command.GetOrderTracking;
import com.epam.delivery.command.TestCreateOrder;
import com.epam.delivery.command.TestDeleteCargo;
import com.epam.delivery.command.TestUpdateCargo;
import com.epam.delivery.util.Either;

public class ActionService {
    private static ActionService instance;
    
    private Map<String, Action> actionMap;
    private ActionService() {
        Function<ServletRequest, Either<Map<String, InputDTO>, List<ActionError>>> im = ActionService::imR;
        Set<UserRole> allowList = new HashSet<>();
        allowList.add(UserRole.CUSTOMER);
        Command getOrderTrack = new GetOrderTracking();
        Command testInsertCommand = new TestCreateOrder();
        Command testUpdateCommand = new TestUpdateCargo();
        Command testDeleteCommand = new TestDeleteCargo();
        
        
        Action orderTrack = new Action();
        orderTrack.setInputMapper(im);
        orderTrack.setAllowList(allowList);
        orderTrack.setCommand(getOrderTrack);
        orderTrack.setSuccessViewName("getOrderTrack");
        
        Action testInsert = new Action();
        testInsert.setInputMapper(ActionService::emptyMapper);
        testInsert.setAllowList(allowList);
        testInsert.setCommand(testInsertCommand);
        testInsert.setSuccessViewName("testInsert");
        
        Action testUpdate = new Action();
        testUpdate.setInputMapper(ActionService::emptyMapper);
        testUpdate.setAllowList(allowList);
        testUpdate.setCommand(testUpdateCommand);
        testUpdate.setSuccessViewName("testUpdate");
        
        Action testDelete = new Action();
        testDelete.setInputMapper(ActionService::emptyMapper);
        testDelete.setAllowList(allowList);
        testDelete.setCommand(testDeleteCommand);
        testDelete.setSuccessViewName("testUpdate");
        
        actionMap = new HashMap<>();
        actionMap.put("getOrderTrack", orderTrack);
        actionMap.put("testInsert", testInsert);
        actionMap.put("testUpdate", testDelete);
        
    }
    
    public static ActionService get() {
        if (instance == null) {
            instance = new ActionService();
        }
        return instance;
    }
    
    public Action findActionByRequest(ServletRequest request) {
        String actionName = request.getParameter("action");
        return actionMap.getOrDefault(actionName, null);
        
    }
    
    private static Either<Map<String, InputDTO>, List<ActionError>> imR(ServletRequest request) {
        List<ActionError> errorList = new ArrayList<>();
        Map<String, InputDTO> result = new HashMap<>();
        
        String paramExternalId = request.getParameter("externalId");
        Either<ExternalId, ActionError> externalID = ExternalId.create(paramExternalId);
        if (externalID.isNormal()) {
            result.put("externalId", externalID.getNormal());
        } else {
            errorList.add(externalID.getAlt());
        }              
        
        return externalID.map(e -> result, e -> errorList);
    }
    
    private static Either<Map<String, InputDTO>, List<ActionError>> emptyMapper(ServletRequest request) {
        return Either.makeNormal(null);
    }
}
