package com.epam.delivery.service;

import java.util.List;

import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.OutputDTO;
import com.epam.delivery.core.View;
import com.epam.delivery.util.Either;
import com.epam.delivery.view.ErrorView;
import com.epam.delivery.view.PageView;

public class ViewService {
    public static View getNoAccessView() {
        return null;
    }

    public static View getView(String name,
            Either<OutputDTO, List<ActionError>> output) {
        if ("getOrderTrack".equals(name)) {
            PageView view = new PageView();
            view.setDelegateMethod(PageView.DelegateMethod.FORWARD);
            view.setPage("/index.jsp");
            view.setOutput(output);
            return view;
        }
        if ("testInsert".equals(name)) {
            PageView view = new PageView();
            view.setDelegateMethod(PageView.DelegateMethod.FORWARD);
            view.setPage("/insert_result.jsp");
            view.setOutput(output);
            return view;
        }
        if ("testUpdate".equals(name)) {
            PageView view = new PageView();
            view.setDelegateMethod(PageView.DelegateMethod.FORWARD);
            view.setPage("/update_result.jsp");
            view.setOutput(output);
            return view;
        }
        return null;
    }

    public static View getErrorView(List<ActionError> errorList) {
        ErrorView view = new ErrorView();
        view.setErrorList(errorList);
        return view;
    }
}
