package com.epam.delivery.command;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.Command;
import com.epam.delivery.core.InputDTO;
import com.epam.delivery.core.OutputDTO;
import com.epam.delivery.data.DTO.output.TestCargoOutputDTO;
import com.epam.delivery.data.domain.Cargo;
import com.epam.delivery.data.domain.CargoType;
import com.epam.delivery.datasource.mysql.MySQLDataService;
import com.epam.delivery.util.Either;
import com.epam.delivery.util.ExceptionWrapper;

public class TestDeleteCargo extends Command {
	@Override
	public Either<OutputDTO, List<ActionError>> perform(Map<String, InputDTO> input) {
		Cargo testCargo = new Cargo();
		testCargo.setId(25);
		testCargo.setMass(new BigDecimal(10000));
		testCargo.setVolume(new BigDecimal(50000));
		testCargo.setType(CargoType.CLOTHES);
		
		return ExceptionWrapper.try$(() -> new MySQLDataService().getCargoRepository())
				.collapse(r -> r.remove(testCargo))
				.map(e -> waypointToOutput("ok"), e -> exceptionToErrorList(e));
	}

	private static OutputDTO waypointToOutput(String ok) {
		TestCargoOutputDTO out = new TestCargoOutputDTO();
		out.setOk(ok);

		return out;
	}
}