package com.epam.delivery.command;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.Command;
import com.epam.delivery.core.InputDTO;
import com.epam.delivery.core.OutputDTO;
import com.epam.delivery.data.DTO.output.TestCargoOutputDTO;
import com.epam.delivery.data.domain.Cargo;
import com.epam.delivery.data.domain.CargoType;
import com.epam.delivery.data.domain.Invoice;
import com.epam.delivery.data.domain.Order;
import com.epam.delivery.service.DataService;
import com.epam.delivery.service.ServiceFactory;
import com.epam.delivery.util.Either;

public class TestCreateOrder extends Command {
	@Override
	public Either<OutputDTO, List<ActionError>> perform(Map<String, InputDTO> input) {
//		long externalId = ((ExternalId) input.get("externalId")).getValue();
		Cargo testCargo = new Cargo();
		testCargo.setMass(new BigDecimal(1000));
		testCargo.setVolume(new BigDecimal(5000));
		testCargo.setType(CargoType.CHEMS);

		Invoice testInvoice = new Invoice();
		testInvoice.setExternalId(1234554321123456L);
		testInvoice.setTotalPrice(new BigDecimal(200000));
		testInvoice.setPaid(true);

		DataService dataService = ServiceFactory.getDataService();
		Order testOrder = new Order();
		testOrder.setCargo(dataService.getCargoRepository().getReference(testCargo));
		testOrder.setExternalId(1234554321123456L);
		testOrder.setFrom(dataService.getWaypointRepository().getReference(3));
		testOrder.setTo(dataService.getWaypointRepository().getReference(8));
		testOrder.setCreator(dataService.getPersonRepository().getReference(8));
		testOrder.setSender(dataService.getPersonRepository().getReference(8));
		testOrder.setReceiver(dataService.getPersonRepository().getReference(5));
		testOrder.setInvoice(dataService.getInvoiceRepository().getReference(testInvoice));

		return dataService.getOrderRepository().add(testOrder).map(TestCreateOrder::testOutput,
				e -> exceptionToErrorList(e));
	}

	private static OutputDTO testOutput(long id) {
		TestCargoOutputDTO out = new TestCargoOutputDTO();
		out.setId(id);

		return out;
	}
}