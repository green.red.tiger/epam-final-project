package com.epam.delivery.command;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.Command;
import com.epam.delivery.core.InputDTO;
import com.epam.delivery.core.OutputDTO;
import com.epam.delivery.data.DTO.input.ExternalId;
import com.epam.delivery.data.DTO.output.OrderTrackOutputDTO;
import com.epam.delivery.data.domain.OrderHistory;
import com.epam.delivery.data.domain.Waypoint;
import com.epam.delivery.service.ServiceFactory;
import com.epam.delivery.util.Either;

public class GetOrderTracking extends Command {
	private static Comparator<OrderHistory> historyComaparator = (oh1, oh2) -> oh1.getDate().compareTo(oh2.getDate());

	@Override
	public Either<OutputDTO, List<ActionError>> perform(Map<String, InputDTO> input) {
		long externalId = ((ExternalId) input.get("externalId")).getValue();

		return ServiceFactory.getDataService().getOrderRepository().filterByExternalId(externalId).getOnly()
				.collapse(order -> order.getHistory().resolve()).then(list -> list.stream().max(historyComaparator))
				.collapse(optHistory -> Either.fromOptional(optHistory, NullPointerException::new))
				.collapse(history -> history.getCurrentLocation().resolve())
				.map(GetOrderTracking::waypointToOutput, e -> exceptionToErrorList(e));
	}

	private static OutputDTO waypointToOutput(Waypoint wp) {
		OrderTrackOutputDTO output = new OrderTrackOutputDTO();
		output.setId(wp.getId());
		output.setCity(wp.getCity().resolve().getNormal().getValue());
		output.setAddress(wp.getAddress());

		return output;
	}
}
