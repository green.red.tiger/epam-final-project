package com.epam.delivery.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.OutputDTO;
import com.epam.delivery.core.View;
import com.epam.delivery.util.Either;

import lombok.Data;

@Data
public class PageView implements View {
    private String page;
    private DelegateMethod delegateMethod;
    private Either<OutputDTO, List<ActionError>> output;
    
    
    @Override
    public void show(ServletRequest request, ServletContext context,
            HttpServletResponse response) throws IOException, ServletException {
        if (output.isNormal()) {
            request.setAttribute("output", output.getNormal());
        } else {
            request.setAttribute("errorList", output.getAlt());
        }
        
        if (delegateMethod.equals(DelegateMethod.FORWARD)) {
            context.getRequestDispatcher(page).forward(request, response);
        }
        else if (delegateMethod.equals(DelegateMethod.REDIRECT)) {
            response.sendRedirect(page);
        }
        
    }
    
    public static enum DelegateMethod {
        FORWARD,
        REDIRECT
    }

}
