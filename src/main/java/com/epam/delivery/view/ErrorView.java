package com.epam.delivery.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.View;
import lombok.Data;

@Data
public class ErrorView implements View {
    private List<ActionError> errorList;
    
    
    @Override
    public void show(ServletRequest request, ServletContext context,
            HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("errorList", errorList);
        context.getRequestDispatcher("/error.jsp").forward(request, response);
        
    }
}
