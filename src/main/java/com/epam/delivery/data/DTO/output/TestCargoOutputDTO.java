package com.epam.delivery.data.DTO.output;

import com.epam.delivery.core.OutputDTO;

import lombok.Data;

@Data
public class TestCargoOutputDTO implements OutputDTO {
	private long id;
	private String ok;
}
