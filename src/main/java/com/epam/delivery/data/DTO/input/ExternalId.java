package com.epam.delivery.data.DTO.input;

import java.util.Objects;

import com.epam.delivery.core.ActionError;
import com.epam.delivery.core.InputDTO;
import com.epam.delivery.util.Either;
import com.epam.delivery.util.ExceptionWrapper;

public class ExternalId implements InputDTO {
	private long value;

	private ExternalId(long externalId) {
		this.value = externalId;
	}

	public long getValue() {
		return value;
	}

	public static Either<ExternalId, ActionError> create(String paramExternalId) {
		return Either.test(!Objects.isNull(paramExternalId), paramExternalId, new NullPointerException())
				.map(p -> p, npe -> new ActionError("External Id is not specified"))
				.collapse(p -> Either.test(p.length() == 16, p,
						new ActionError("External Id must be 16 characters long")))
				.collapse(p -> ExceptionWrapper.try$(() -> Long.parseLong(p)).map(e -> new ExternalId(e),
						e -> new ActionError("External Id must contain only digits")));
//        if (paramExternalId.length() != 16) {
//            return Either.makeAlt(new ActionError("External Id must be 16 characters long"));
//        }
//        try {
//            long externalId = Long.parseLong(paramExternalId);
//            return Either.makeNormal(new ExternalId(externalId));
//        }
//        catch (Exception e) {
//            return Either.makeAlt(new ActionError("External Id must contain only digits"));
//        }
	}
}
