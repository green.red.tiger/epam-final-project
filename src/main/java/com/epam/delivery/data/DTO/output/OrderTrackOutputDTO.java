package com.epam.delivery.data.DTO.output;

import com.epam.delivery.core.OutputDTO;

import lombok.Data;

@Data
public class OrderTrackOutputDTO implements OutputDTO {
	private long id;
	private String city;
	private String address;
}
