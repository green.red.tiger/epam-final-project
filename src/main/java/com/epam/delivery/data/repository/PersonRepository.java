package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Person;

public interface PersonRepository extends GenericRepository<Person>{
	
}
