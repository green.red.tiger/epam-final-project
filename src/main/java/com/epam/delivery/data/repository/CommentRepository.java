package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Comment;

public interface CommentRepository extends GenericRepository<Comment>{
	
}
