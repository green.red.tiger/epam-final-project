package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Order;
import com.epam.delivery.data.domain.OrderHistory;

public interface OrderHistoryRepository extends GenericRepository<OrderHistory>{
	public OrderHistoryRepository filterByOrder(Order order);
}
