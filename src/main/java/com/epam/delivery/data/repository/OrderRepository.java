package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Order;

public interface OrderRepository extends GenericRepository<Order>{
	OrderRepository filterByExternalId(long externalId);
}
