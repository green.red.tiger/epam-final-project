package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Cargo;
import com.epam.delivery.data.domain.City;

public interface CityRepository extends GenericRepository<City>{
	
}
