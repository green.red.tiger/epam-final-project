package com.epam.delivery.data.repository;

import com.epam.delivery.core.entity.GenericRepository;
import com.epam.delivery.data.domain.Cargo;

public interface CargoRepository extends GenericRepository<Cargo>{
	
}
