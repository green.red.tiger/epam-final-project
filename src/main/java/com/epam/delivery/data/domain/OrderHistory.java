package com.epam.delivery.data.domain;

import java.time.LocalDate;

import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
public class OrderHistory extends Storable {
    private long id;
    private Reference<Waypoint> currentLocation;
    private int state;
    private LocalDate date;
    private Reference<Comment> comment;
    
	@Override
	public void actualizeRefs() {
		getComment().actualize();
	}
}
