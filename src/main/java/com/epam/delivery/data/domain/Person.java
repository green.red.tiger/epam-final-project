package com.epam.delivery.data.domain;

import java.time.LocalDateTime;

import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
public class Person extends Storable {
    private long id;
    private String name;
    private String patronymic;
    private String surname;
    private LocalDateTime birthdate;
    private String email;
    private long phone;
    
	@Override
	public void actualizeRefs() {
		return;
	}
}
