package com.epam.delivery.data.domain;

import java.math.BigDecimal;

import com.epam.delivery.core.entity.Attribute;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.core.entity.StorageState;

import lombok.Data;

@Data
public class City extends Storable {
	private long id;
	private String value;
	
	@Override
	public void actualizeRefs() {
		return;
	}
}
