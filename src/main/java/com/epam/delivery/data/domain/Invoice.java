package com.epam.delivery.data.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.epam.delivery.core.entity.Attribute;
import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
public class Invoice extends Storable {
	@Attribute private long id;
	@Attribute private long externalId;
	@Attribute private BigDecimal totalPrice;
	@Attribute private boolean isPaid;
	
	@Override
	public void actualizeRefs() {
		return;
	}
}
