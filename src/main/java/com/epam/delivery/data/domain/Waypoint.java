package com.epam.delivery.data.domain;

import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
public class Waypoint extends Storable {
    private long id;
    private Reference<City> city;
    private double longitude;
    private double latitude;
    private String address;
    
	@Override
	public void actualizeRefs() {
		getCity().actualize();
	}
}
