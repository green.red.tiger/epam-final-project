package com.epam.delivery.data.domain;

import com.epam.delivery.core.entity.Entity;
import com.epam.delivery.core.entity.Reference;

import lombok.Data;

@Data
public class Employee extends Person {
    private Reference<Waypoint> waypoint;
}
