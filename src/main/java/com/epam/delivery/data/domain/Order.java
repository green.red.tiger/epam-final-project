package com.epam.delivery.data.domain;

import java.math.BigInteger;
import java.util.List;

import com.epam.delivery.core.entity.ListReference;
import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
public class Order extends Storable {
    private long id;
    private Reference<Cargo> cargo;
    private Long externalId;
    private Reference<Waypoint> from;
    private Reference<Waypoint> to;
    private Reference<Person> creator;
    private Reference<Person> sender;
    private Reference<Person> receiver;
    private Reference<Invoice> invoice;
    private ListReference<OrderHistory> history;
    
	@Override
	public void actualizeRefs() {
		getCargo().actualize();
		getFrom().actualize();
		getTo().actualize();
		getCreator().actualize();
		getSender().actualize();
		getReceiver().actualize();
		getInvoice().actualize();
	}
}
