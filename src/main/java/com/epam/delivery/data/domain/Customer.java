package com.epam.delivery.data.domain;

import java.math.BigInteger;

import lombok.Data;

@Data
public class Customer extends Person {
    private BigInteger externalId;
    private boolean isVerified;
}
