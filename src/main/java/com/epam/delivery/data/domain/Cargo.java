package com.epam.delivery.data.domain;

import java.math.BigDecimal;

import com.epam.delivery.core.entity.Attribute;
import com.epam.delivery.core.entity.Storable;
import com.epam.delivery.core.entity.StorageState;

import lombok.Data;

@Data
public class Cargo extends Storable {
	@Attribute("id_cargo") private long id;
	@Attribute private BigDecimal mass;
	@Attribute private BigDecimal volume;
	@Attribute private CargoType type;
	
	@Override
	public void actualizeRefs() {
		return;
	}
}
