package com.epam.delivery.data.domain;

import java.util.HashMap;
import java.util.Map;

public enum CargoType {
    MACHINES(1), CHEMS(2), MEDICAL(3), FOOD(4), CLOTHES(5);

    private final int typeId;

    private static final Map<Integer, CargoType> map = new HashMap<>();
    static {
        for (CargoType c : CargoType.values()) {
            map.put(c.typeId, c);
        }
    }
    
    private CargoType(final int typeId) {
        this.typeId = typeId;
    }

    public static CargoType valueOf(int legNo) {
        return map.get(legNo);
    }
    
    public int getValue() {
        return typeId;
    }
}
