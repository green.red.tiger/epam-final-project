package com.epam.delivery.data.domain;

import java.time.LocalDateTime;

import com.epam.delivery.core.entity.Attribute;
import com.epam.delivery.core.entity.Entity;
import com.epam.delivery.core.entity.Reference;
import com.epam.delivery.core.entity.Storable;

import lombok.Data;

@Data
@Entity("comment")
public class Comment extends Storable {
	@Attribute("id_comment")private long id;
	@Attribute private Reference<Person> author;
	@Attribute private LocalDateTime creationDate;
	@Attribute private LocalDateTime editDate;
	@Attribute private String text;
	
	@Override
	public void actualizeRefs() {
		getAuthor().actualize();
	}
}
