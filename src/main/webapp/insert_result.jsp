<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Application</title>
</head>
<body>
<c:if test="${not empty output}">
	<p>Id: ${output.id} </p>
</c:if>
	<c:if test="${not empty errorList}">
		<c:forEach var="error" items="${errorList}">
		<p>${error.message}!</p></br>
	</c:forEach>
</c:if>

</body>
</html>