<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Java Bean Page</title>
</head>
<body>
<div>
<c:forEach var="error" items="${errorList}">
	<p>${error.message}!</p>
</c:forEach>
</div>
</body>
</html>