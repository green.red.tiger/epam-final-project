-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: delivery
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id_account` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registration_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth-credentials`
--

DROP TABLE IF EXISTS `auth-credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth-credentials` (
  `id_account` int(10) unsigned NOT NULL,
  `username` varchar(45) NOT NULL,
  `password_sha_3_256` varchar(45) NOT NULL,
  KEY `fk_auth-credentials_1_idx` (`id_account`),
  CONSTRAINT `fk_auth-credentials_1` FOREIGN KEY (`id_account`) REFERENCES `account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth-credentials`
--

LOCK TABLES `auth-credentials` WRITE;
/*!40000 ALTER TABLE `auth-credentials` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth-credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id_cargo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mass` decimal(10,0) NOT NULL,
  `volume` decimal(10,0) NOT NULL,
  `id_cargo_type` int(11) NOT NULL,
  PRIMARY KEY (`id_cargo`),
  KEY `fk_cargo_1_idx` (`id_cargo_type`),
  CONSTRAINT `fk_cargo_1` FOREIGN KEY (`id_cargo_type`) REFERENCES `cargo_type` (`id_cargo_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (2,500,1,0),(3,8500,1700,1),(4,2000,400,4),(5,4500,900,1),(6,6500,1625,3),(7,5000,2500,1),(8,6500,2167,1),(9,500,100,3),(10,4000,4000,3),(11,2000,400,1),(12,3000,3000,1),(13,1000,200,1),(14,5500,1833,0),(15,8000,1600,1),(16,9000,1800,0),(17,5500,5500,1),(18,10000,5000,3),(19,2000,2000,4),(20,5500,5500,0),(21,1000,1000,2);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo_type`
--

DROP TABLE IF EXISTS `cargo_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo_type` (
  `id_cargo_type` int(11) NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`id_cargo_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo_type`
--

LOCK TABLES `cargo_type` WRITE;
/*!40000 ALTER TABLE `cargo_type` DISABLE KEYS */;
INSERT INTO `cargo_type` VALUES (0,'Станки и машины'),(1,'Удобрения и химикаты'),(2,'Медицинское оборудование'),(3,'Продукты питания'),(4,'Одежда и обувь');
/*!40000 ALTER TABLE `cargo_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (0,'Киев'),(1,'Чернигов'),(2,'Одесса'),(3,'Львов');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id_comment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` int(10) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id_comment`),
  KEY `fk_comment_1_idx` (`author`),
  CONSTRAINT `fk_comment_1` FOREIGN KEY (`author`) REFERENCES `person` (`id_person`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distance_cost`
--

DROP TABLE IF EXISTS `distance_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distance_cost` (
  `from` int(11) unsigned NOT NULL,
  `to` int(11) unsigned NOT NULL,
  `value` decimal(10,0) unsigned NOT NULL,
  PRIMARY KEY (`from`,`to`),
  KEY `fk_distance_cost_2` (`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distance_cost`
--

LOCK TABLES `distance_cost` WRITE;
/*!40000 ALTER TABLE `distance_cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `distance_cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id_invoice` int(11) unsigned NOT NULL,
  `external_id` bigint(20) unsigned NOT NULL,
  `total_price` decimal(10,0) unsigned NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (0,4164797307950540,26857,1),(1,6168520984973623,25655,1),(2,2852340495139169,23411,1),(3,7649693078319013,28850,1),(4,6964637569598299,10242,1),(5,8590484118942515,25133,1),(6,7199560354558133,23077,0),(7,9716994025346650,11792,0),(8,8985264250332990,16484,1),(9,6029227715904977,18076,1),(10,2654282078599919,15245,1),(11,9760139660642590,27976,0),(12,4854014391139799,16408,1),(13,2146225044460737,12192,1),(14,2422994768944682,22538,1),(15,8093246610116439,15465,0),(16,5604048098181945,16773,0),(17,6737697069765574,28735,1),(18,4831793074728538,13341,1),(19,5665508410806111,16632,1);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cargo` int(10) unsigned NOT NULL,
  `external_id` bigint(20) unsigned NOT NULL,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `creator` int(10) unsigned DEFAULT NULL,
  `sender` int(10) unsigned NOT NULL,
  `receiver` int(10) unsigned NOT NULL,
  `id_invoice` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `fk_order_1_idx` (`id_cargo`),
  KEY `fk_order_2_idx` (`from`),
  KEY `fk_order_3_idx` (`to`),
  KEY `fk_order_4_idx` (`creator`),
  KEY `fk_order_5_idx` (`sender`),
  KEY `fk_order_6_idx` (`receiver`),
  KEY `fk_order_7_idx` (`id_invoice`),
  CONSTRAINT `fk_order_1` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id_cargo`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_2` FOREIGN KEY (`from`) REFERENCES `waypoint` (`id_waypoint`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_3` FOREIGN KEY (`to`) REFERENCES `waypoint` (`id_waypoint`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_4` FOREIGN KEY (`creator`) REFERENCES `person` (`id_person`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_order_5` FOREIGN KEY (`sender`) REFERENCES `person` (`id_person`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_6` FOREIGN KEY (`receiver`) REFERENCES `person` (`id_person`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_7` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id_invoice`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (2,2,6103665463542801,5,10,0,0,2,0),(3,3,5914334398357978,7,5,3,3,1,1),(4,4,3011089483959510,10,8,0,0,7,2),(5,5,7572240412286492,8,8,1,1,5,3),(6,6,2386802994777470,3,5,0,0,1,4),(7,7,7747075470122133,9,8,6,6,1,5),(8,8,2637757103161090,6,3,5,5,2,6),(9,9,2536094625156617,10,9,9,9,7,7),(10,10,9005434667251373,8,5,6,6,7,8),(11,11,2648612025643523,7,5,2,2,8,9),(12,12,9162491829671170,1,7,9,9,1,10),(13,13,3915764339388202,4,10,7,7,2,11),(14,14,3263824168931767,9,8,3,3,9,12),(15,15,7129806693383381,10,5,1,1,2,13),(16,16,8868517817305799,9,5,9,9,4,14),(17,17,5701951869112337,2,4,0,0,3,15),(18,18,3938329775351235,9,3,6,6,5,16),(19,19,4604415567888464,6,10,5,5,3,17),(20,20,1482440976526508,6,5,3,3,2,18),(21,21,1642015287961117,8,1,8,8,9,19);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_history`
--

DROP TABLE IF EXISTS `order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_history` (
  `id_order_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `current_location` int(10) unsigned NOT NULL,
  `state` tinyint(3) unsigned NOT NULL,
  `id_comment` int(10) unsigned DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id_order_history`),
  KEY `fk_order_history_1_idx` (`id_order`),
  KEY `fk_order_history_2_idx` (`current_location`),
  KEY `fk_order_history_3_idx` (`id_comment`),
  CONSTRAINT `fk_order_history_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_order_history_2` FOREIGN KEY (`current_location`) REFERENCES `waypoint` (`id_waypoint`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_history_3` FOREIGN KEY (`id_comment`) REFERENCES `comment` (`id_comment`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_history`
--

LOCK TABLES `order_history` WRITE;
/*!40000 ALTER TABLE `order_history` DISABLE KEYS */;
INSERT INTO `order_history` VALUES (1,2,1,0,NULL,'2021-06-06 00:00:00');
/*!40000 ALTER TABLE `order_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id_person` int(10) unsigned NOT NULL,
  `id_account` int(10) unsigned DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `patronymic` varchar(45) DEFAULT NULL,
  `surname` varchar(45) NOT NULL,
  `birthdate` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `id_role` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_person`),
  KEY `fk_person_2_idx` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (0,0,'Михайлов','Владислав','Олегович','1990-12-12 00:00:00','',0,0),(1,NULL,'Рябов','Виктор','Андреевич','1986-11-12 00:00:00','',0,0),(2,NULL,'Кондратьев','Максим','Олегович','1995-12-09 00:00:00',NULL,NULL,0),(3,NULL,'Логинов','Семен','Леонидович','1991-07-22 00:00:00',NULL,NULL,0),(4,NULL,'Карпов','Борис','Ростиславович','1987-08-09 00:00:00',NULL,NULL,0),(5,NULL,'Симонов','Арсен','Русланович','1992-03-19 00:00:00',NULL,NULL,0),(6,NULL,'Кулаков','Андрей','Викторович','1994-08-02 00:00:00',NULL,NULL,0),(7,NULL,'Беляков','Денис','Юрьевич','1986-01-30 00:00:00',NULL,NULL,0),(8,NULL,'Петров','Илья','Владимирович','2000-08-28 00:00:00',NULL,NULL,0),(9,NULL,'Жданов','Владислав','Геннадьевич','1994-12-24 00:00:00',NULL,NULL,0);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person-customer`
--

DROP TABLE IF EXISTS `person-customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person-customer` (
  `id_person` int(10) unsigned NOT NULL,
  `external_id` bigint(20) unsigned NOT NULL,
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_person`),
  CONSTRAINT `fk_person-customer_1` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person-customer`
--

LOCK TABLES `person-customer` WRITE;
/*!40000 ALTER TABLE `person-customer` DISABLE KEYS */;
INSERT INTO `person-customer` VALUES (0,5984714295752934,1),(3,6002633659075567,1),(4,7196399332102749,1),(6,7786318845873479,1),(7,2498907823757900,1);
/*!40000 ALTER TABLE `person-customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person-employee`
--

DROP TABLE IF EXISTS `person-employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person-employee` (
  `id_person` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `office` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_person`),
  KEY `fk_person-employee_2_idx` (`office`),
  CONSTRAINT `fk_person-employee_1` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person-employee_2` FOREIGN KEY (`office`) REFERENCES `waypoint` (`id_waypoint`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person-employee`
--

LOCK TABLES `person-employee` WRITE;
/*!40000 ALTER TABLE `person-employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `person-employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id_role` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'guest','Default user role');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waypoint`
--

DROP TABLE IF EXISTS `waypoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waypoint` (
  `id_waypoint` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_city` int(11) NOT NULL,
  `longitude` decimal(10,0) NOT NULL,
  `latitude` decimal(10,0) NOT NULL,
  `address` varchar(200) NOT NULL,
  PRIMARY KEY (`id_waypoint`),
  KEY `fk_waypoint_1` (`id_city`),
  CONSTRAINT `fk_waypoint_1` FOREIGN KEY (`id_city`) REFERENCES `city` (`id_city`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waypoint`
--

LOCK TABLES `waypoint` WRITE;
/*!40000 ALTER TABLE `waypoint` DISABLE KEYS */;
INSERT INTO `waypoint` VALUES (1,3,50,24,'2A, вулиця Грінченка'),(2,0,50,30,'ул. Дегтярёвская, 58'),(3,2,46,31,'вулиця Розумовська, 10/12'),(4,3,50,24,'вулиця Данила Апостола, 16'),(5,0,50,30,'вулиця Литвиненко-Вольгемут, 1Г'),(6,1,51,31,'вулиця Ушинського, 9'),(7,2,47,31,'Балтська дорога, 71'),(8,0,50,31,'вулиця Євгенія Харченка, 29'),(9,0,50,31,'вулиця Братиславська, 11'),(10,0,51,30,'вул. Мінське Шосе, Мінський проспект, 10а');
/*!40000 ALTER TABLE `waypoint` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-30  5:24:38
